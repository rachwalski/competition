
#include "TomoSynth.h"

#include "TomoSynth.h"
#include "WeightTables.h"
#include "trt_utils.h"
#include "EdgeGainCorrect.h"
#include <stdio.h>
#include <stddef.h>
#include "Reconstruct.h"
#include "SensorInfo.h"

#include "ReconstructParallel.h"


#if RECONSTRUCT_PARALLEL
float **reconstruct(nova_str *nova, void *llhData, float ****weight, void *nor, float **h_nor)
#else
float **reconstruct(nova_str *nova, uInt8 ****llhData, float **** weight,
		float ** nor)
#endif
{
	float **reconim;
	int idx;
	int rpos, cpos;
	int r, c, i, j, w;
	uint32_t Mr, Mc;
	Mr = M * nova->row;
	Mc = M * nova->col;

	reconim = (float **) create_2D_float(Mr, Mc);

	printf("        M=%i\n", M);

	/* Zero out image buffer */
	for (r = 0; r < Mr; r++) {
		for (c = 0; c < Mc; c++) {
			reconim[r][c] = 0.0;
		}
	}

#if RECONSTRUCT_PARALLEL

	printf("Reconstructing plane on GPU...\n");
	reconstruct_parallel(nova, reconim, llhData, weight, nor);

#else
	printf("Reconstructing plane sequentially...\n");

	for (r = 0; r < nova->row; r++) {
		for (c = 0; c < nova->col; c++) {

			if (nor[r][c] > 0.0) {

				for (i = 0; i < nova->detsizex; i++) {
					for (j = 0; j < nova->detsizey; j++) {
						for (w = 0; w < 4; w++) {
							rpos = (weight[i][j][w][0]) + (r) * (M) + M_HALF;

							if ((rpos > 0) && (rpos <= M * nova->row)) {
								cpos = (weight[i][j][w][1]) + (c) * (M)+ M_HALF;
								if ((cpos > 0 && (cpos <= M * nova->col))) {
									rpos = rpos - 1;
									cpos = cpos - 1;

									reconim[rpos][cpos] = reconim[rpos][cpos] + llhData[i][j][r][c] * weight[i][j][w][2];
								} /* if cpos */
							} /* if rpos */
						} /* for w */
					} /* for j */
				} /* for i */
			} /* if nor */
		} /* for c */
	} /* for r */

#endif

#if EDGE_GAIN_CORRECTION

	float* normim;

#if RECONSTRUCT_PARALLEL
	normim = initEdgeGain(nova, weight, h_nor);
#else
	normim = initEdgeGain(nova, weight, nor);
#endif

	for (r = 0; r < Mr; r++) {
		for (c = 0; c < Mc; c++) {
			idx = r * Mc + c;
			if (normim[idx] > 0.0) {
				reconim[r][c] = reconim[r][c] / normim[idx];
			}
		}
	}

	free(normim);

#endif

	printf("RETURNING\n");
	fflush(stdout);

	return reconim;

}

