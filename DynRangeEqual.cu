
#include "TomoSynth.h"
#include "trt_utils.h"
#include "DynRangeEqual.h"

float** DynRangeEqualization(nova_str *nova , uint8_t ****llhData)
{
    int r, c, j, k;
    float **nor;
    float tmpDbg, tmp_sum;
    float nor_thresh;
    float maxDet,maxNor;
    float p;
    nor = create_2D_float(nova->col , nova->row);


    //thr=sum(sum(llhData(:,:,2,2),1),2)/(sizedet(1)*sizedet(2))*1.05;
    tmp_sum = 0.0;
    for(j = 0; j < nova->detsizex; j++)
    {
        for(k = 0; k < nova->detsizey; k++)
        {
            for(r = 0; r < nova->row; r++)
            {
                for(c = 0; c < nova->col; c++)
                {

                    if (llhData[j][k][r][c] > PIX_THRESH)
                    { // Remove Bad Pixels
                        printf("llhData[%i][%i][%i][%i]= %u, marked as bad\n" ,
                                j , k , r , c , llhData[j][k][r][c]);
                        llhData[j][k][r][c] = 0;
                    }
           //         tmp_sum += llhData[j][k][1][1];
                }
            }
            tmp_sum += llhData[j][k][1][1];
        }
    }
    nor_thresh = NOR_SCALE * tmp_sum / (nova->detsizex * nova->detsizey);
    //nor_thresh=0.6;

    for(r = 0; r < nova->row; r++)
    {
        for(c = 0; c < nova->col; c++)
        {
            // Compute Mean
            nor[r][c] = 0.0;
            tmpDbg = 0.0;
            maxDet=0.0;
            for(j = 0; j < nova->detsizex; j++)
            {
                for(k = 0; k < nova->detsizey; k++)
                {

                    tmpDbg += llhData[j][k][r][c];
                    nor[r][c] += llhData[j][k][r][c];
                    if( llhData[j][k][r][c]>maxDet){
                    		maxDet=llhData[j][k][r][c];
                    }
                }
            }

            if (nor[r][c]>0){
            	p=maxDet/nor[r][c];
            	if (p > maxNor){
            		maxNor=p;
            	}
            }
        }
    }
     //       nor[r][c] /= (float) (nova->detsizex * nova->detsizey);

            float s;
            // Equalize

    for(r = 0; r < nova->row; r++)
    {
        for(c = 0; c < nova->col; c++)
        {
            if (nor[r][c] > nor_thresh)
            {
                if (DRE == 1) // If dynamic Range Equalization is enabled, do it.
                {
                    s = PIX_MAX / (nor[r][c] * maxNor);
//                    printf("s=%f\n" , s);
                    for(j = 0; j < nova->detsizex; j++)
                    {
                        for(k = 0; k < nova->detsizey; k++)
                        {

                            llhData[j][k][r][c] = (uint8_t) (s
                                    * llhData[j][k][r][c]);

                        }
                    }
                }
            }
            else // If not doing DRE, zero out bad pixels
            {
                nor[r][c] = 0.0;
                for(j = 0; j < nova->detsizex; j++)
                {
                    for(k = 0; k < nova->detsizey; k++)
                    {
                        llhData[j][k][r][c] = 0;
                    }
                }
            }
        }

    }
#ifdef VERBOSE
    for (r = 0; r < nova->row; r++)
    {
        for (c = 0; c < nova->col; c++)
        {
            printf("nor[%i][%i]=%f ",r,c,nor[r][c]);
        }
        printf("\n");
    }
#endif

    return nor;
}

