#include "ReconstructParallel.h"


__global__ void reconstruct_kernel(int *rows, int *cols, float *reconim, 
							uInt8 *llhData, float *weight, float *nor)
{
#if 1  
	int r, c, w;
	int rpos, cpos;
	int lidx, ridx, widx;

    int i = threadIdx.y + blockDim.y * blockIdx.y;
    int j = threadIdx.x + blockDim.x * blockIdx.x;

    if (i < DET_Y && j < DET_X) {

        for (r = 0; r < (*rows); r++) {
            for (c = 0; c < (*cols); c++) {
				if(nor[r*(*rows) + c] > 0.0){
				/* */
				for (w = 0; w < 4; w++) {
					idx_weight(&widx, *rows, *cols, i, j, w, 0);	
					rpos = weight[widx] + r * M + M_HALF;

					if (rpos > 0 && (rpos <= M * (*rows))) {

						idx_weight(&widx, *rows, *cols, i, j, w, 1);
						cpos = weight[widx] + c * M + M_HALF;
						if (cpos > 0 && (cpos <= M * (*cols))) {
							rpos = rpos - 1;
							cpos = cpos - 1;

							idx_llhdata(&lidx, *rows, i, j, r, c);
							idx_reconim(&ridx, *rows, i, j);
							idx_weight(&widx, *rows, *cols, i, j, w, 2);
							reconim[ridx] = reconim[ridx] + llhData[lidx] * weight[widx];
						} /* if cpos */
					} /* if rpos */
				} /* for w */
			}
            } /* end for c */
        } /* end for r */
    } /* end if i */
#endif
}

void reconstruct_parallel(struct nova_str *nova, float **reconim, 
					void *llhData, float ****weight, void *nor)
{
	float *d_reconim;
	float *d_weight;
	int *d_c;	
	int *d_r;
	
	size_t reconimAllocSz = (M * nova->row) * (M * nova->col) * sizeof(float);
	size_t weightAllocSz = nova->detsizex * nova->detsizey * 4 * 3 * sizeof(float);

	cudaMallocSafe((void **) &d_reconim, reconimAllocSz);
	cudaMallocSafe((void **) &d_weight, weightAllocSz);	
	cudaMallocSafe((void **) &d_c, sizeof(uint32_t));
	cudaMallocSafe((void **) &d_r, sizeof(uint32_t)); 
	
	cudaMemcpyToDeviceSafe((void *) d_reconim, (void *) *reconim, reconimAllocSz);
	cudaMemcpyToDeviceSafe((void *) d_weight, (void *) *(*(*weight)), weightAllocSz);
	cudaMemcpyToDeviceSafe((void *) d_c, &(nova->col), sizeof(uint32_t));
	cudaMemcpyToDeviceSafe((void *) d_r, &(nova->row), sizeof(uint32_t));

	int block_x = nova->detsizex / BLOCK_SIZE;
	if (nova->detsizex % BLOCK_SIZE)
		++block_x;

	int block_y = nova->detsizey / BLOCK_SIZE;
	if (nova->detsizey % BLOCK_SIZE)
		++block_y;

	dim3 dimGrid(1, 1, 1);
	dim3 dimBlock(block_x, block_y, 1);

	reconstruct_kernel<<< dimGrid, dimBlock >>>(d_r, d_c, d_reconim, (uInt8 *) llhData, d_weight, (float *) nor);

	/* Copy the result back from the GPU */	
	cudaMemcpyToHostSafe((void *) *reconim, (void *) d_reconim, reconimAllocSz);
	
	/* free any temporary device allocations */
	cudaFree(d_r);
	cudaFree(d_c);
	cudaFree(d_weight);
	cudaFree(d_reconim);
}

__device__ void idx_llhdata(int *out, int rows, int i, int j, int r, int c)
{
	*out = (DET_X * i + DET_Y * j + rows * r + c);
}

__device__ void idx_reconim(int *out, int rows, int i, int j)
{
	*out = (M * rows * i + M * j);
}

__device__ void idx_weight(int *out, int rows, int cols, int i, int j, int w, int x)
{
	*out = (rows * i + cols * j + W * w + x);
}


