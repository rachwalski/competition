#ifndef PARA_RECONSTRUCT_H
#define PARA_RECONSTRUCT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TomoSynth.h"
#include "Utils.h"


#define BLOCK_SIZE 32

#define W 4

#define M 10
#define M_HALF (M/2)

#define DET_X 160
#define DET_Y 80

//extern void


/*
 * Function that will set up the kernel to be executed on the device.
 * This includes memory allocation and host to device or device to host 
 * memory copies.
 * @param rows
 * @param cols
 * @param reconim
 * @param llhData
 * @param weight
 * @param nor
 */
void reconstruct_parallel(struct nova_str *nova, float **reconim, 
					void *llhData, float ****weight, void *nor);

/*
 * This function will perform the reconstruction of the image on the GPU.
 * @param rows
 * @param cols
 * @param reconim
 * @param llhData
 * @param weight
 * @param nor
 */
__global__ void reconstruct_kernel(int *rows, int *cols, float *reconim,
					uInt8 *llhData, float *weight, float *nor);

/**
 * Calculates the index into the llhdata array.
 * @param rows
 * @param i
 * @param j
 * @param r
 * @param c
 * @return
 */
__device__ void idx_llhdata(int *out, int rows, int i, int j, int r, int c);

/**
 * Calculates the index into the reconim array.
 * @param rows
 * @param i
 * @param j
 * @return
 */
__device__ void idx_reconim(int *out, int rows, int i, int j);

/**
 * Calculates the index into the weight array.
 * @param rows
 * @param cols
 * @param i
 * @param j
 * @param w
 * @param x
 * @return
 */
__device__ void idx_weight(int *out, int rows, int cols, int i, int j, int w, int x);


#ifdef __cplusplus
}
#endif

#endif
