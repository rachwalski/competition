#ifndef UTILS_H
#define UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#include "TomoSynth.h"

/*
 * Macro to print out an error message and exit the program. 
 */
#define FATAL(msg, ...) \
    do { \
        fprintf(stderr, "[%s:%d] "msg"\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        exit(-1); \
    } while(0)


extern void *d_llhData;
extern void *d_nor;


/**
 * Frees any of the global device pointers that were allocated.
 */
void cudaCleanup();

/**
 * Initializes the llhData and nor device pointers.
 * @param llhData
 * @param nor
 * @param llhSz
 * @param norSz
 */
void cudaInit(uInt8 ****llhData, float **nor, size_t llhSz, size_t norSz);

/**
 * malloc's memory on the device and exist the program on error.
 * @param ptr device ptr to allocate to
 * @param sz the size of the allocation in bytes
 */
void cudaMallocSafe(void **ptr, size_t sz);

/**
 * Copies data from the host to the device and exits the program on error.
 * @param dst device pointer to copy to
 * @param src host pointer to copy from
 * @param sz the number of bytes to copy
 */
void cudaMemcpyToDeviceSafe(void *dst, void *src, size_t sz);

/**
 * Copies data from the device to the host and exits the program on error.
 * @param dst device pointer to copy to
 * @param src host pointer to copy from
 * @param sz the number of bytes to copy
 */
void cudaMemcpyToHostSafe(void *dst, void *src, size_t sz);


#ifdef __cplusplus
}
#endif

#endif
