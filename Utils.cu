#include "Utils.h"

/*
extern void *d_llhData;
extern void *d_nor;
*/

void cudaCleanup()
{
/*
	cudaFree(d_nor);
	cudaFree(d_llhData);

	d_nor = NULL;
	d_llhData = NULL;
*/
}

void cudaInit(uInt8 ****llhData, float **nor, size_t llhSz, size_t norSz)
{
/*	cudaMallocSafe((void **) &d_llhData, llhSz * sizeof(float));
	cudaMallocSafe((void **) &d_nor, norSz * sizeof(float));

	cudaMemcpyToDeviceSafe(d_llhData, (void *) *(*(*llhData)), llhSz * sizeof(float));
	cudaMemcpyToDeviceSafe(d_nor, (void *) *nor, norSz * sizeof(float));
*/
}

void cudaMallocSafe(void **ptr, size_t sz)
{
	cudaError_t cudaRet;
	
	cudaRet = cudaMalloc(ptr, sz);
	if (cudaRet != cudaSuccess)
		FATAL("Failed to allocate data");
}

void cudaMemcpyToDeviceSafe(void *dst, void *src, size_t sz)
{
	cudaError_t cudaRet;

	cudaRet = cudaMemcpy(dst, src, sz, cudaMemcpyHostToDevice);
	if (cudaRet != cudaSuccess)
		FATAL("Failed to copy data to device");
}

void cudaMemcpyToHostSafe(void *dst, void *src, size_t sz)
{
	cudaError_t cudaRet;

	cudaRet = cudaMemcpy(dst, src, sz, cudaMemcpyDeviceToHost);
	if (cudaRet != cudaSuccess)
		FATAL("Failed to copy array to host");
}

