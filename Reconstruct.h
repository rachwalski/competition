
#ifndef RECONSTRUCT_H_
#define RECONSTRUCT_H_

#define ANGLE_DEGREES (-1.426)
#define PI 3.141592653589793
#define DROTATION ANGLE_DEGREES*PI/180.0
#define NUM_PLANES 96
#define DELTA_D_SP 0.2

#define D_SP 35.4  // (plane_z) source to focal-plane distance


#if RECONSTRUCT_PARALLEL
float **reconstruct(nova_str *nova, void *llhData, float ****w, void *nor, float **h_nor);
#else
float**  reconstruct(nova_str* nova, uInt8**** llhData, float**** w, float** nor);
#endif
float * reconstruct_gather(nova_str *nova , detector* d , uInt8 ****llhData , float **** weight , float ** nor, float d_sp);

#endif /* RECONSTRUCT_H_ */
